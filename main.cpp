// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
//
// @author : Arun <arun@uni-bremen.de>
//
//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------

#include <iostream>
#include <string>
#include <ctype.h>
#include <locale>
#include <algorithm>
#include <cassert>
#include <boost/multiprecision/cpp_int.hpp>
#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/dynamic_bitset.hpp>
#include <boost/algorithm/string/case_conv.hpp>
#include <boost/algorithm/string.hpp>
#include <cstdlib>

#include <numbers.hpp>
#include <arith.hpp>
#include <logical.hpp>
#include <utils.hpp>
#include <boolean.hpp>

namespace
{

const std::string usage = std::string("[i] Usage: boolt <num1 OP num2>  (Prefix 0x or 0b to denote hex/binary, and '-' or '~' for negation)\n") +
                          std::string("           Try boolt -h for detailed help \n");

const std::string details = std::string("[i] Usage: boolt <num1 OP num2>  (Prefix 0x or 0b to denote hex/binary, and '-' or '~' for negation)\n") +
                            std::string("           Use \" to quote the arguments. \n") +
                            std::string("           Supported OP : +, -, *  (Mathematical addition, subtraction, multiplication) \n") +
                            std::string("                        : &, |, ^  (Boolean AND, OR, XOR) \n") +
                            std::string("                        : cof0, cof1, diff, forall, exists (Boolean Cofactors, Difference, Exists and ForAll Quantifiers) \n\n") +
                            std::string("    Examples\n") +
                            std::string("     $ boolt \"0x33 + 0x22\"        :Add two numbers 0x33 and 0x22\n") +
                            std::string("     $ boolt \"0x33 & 0x22\"        :Boolean AND of two numbers\n") +
                            std::string("     $ boolt \"0x33 ^ -0x2f\"       :Boolean XOR of first number with inverted second number\n") +
                            std::string("     $ boolt \"0x33 ^ ~0x22\"       :Same as above\n") +
                            std::string("     $ boolt \"-0x33 | ~0b1010\"    :Boolean NAND of 0x33 and 0b1010\n") +
                            std::string("     $ boolt \"0xA1 cof0 1\"        :Cofactor0 of 0xA1 wrt., variable-1\n") +
                            std::string("     $ boolt \"0xA1 diff 0\"        :Boolean difference of 0xA1 wrt., variable-0\n") +
                            std::string("     $ boolt \"0xABC forall 2\"     :Universal quantifier of 0xABC wrt., variable-2\n") +
                            std::string("     $ boolt  -h                  :Displays this help\n\n") +
                            std::string("    For cof0, cof1, diff, exists, forall: the first argument is the truth-table of the function.\n") +
                            std::string("    If there are N inputs, variables start from 0 to N-1. Maximum number of variables allowed is 8.\n\n");

//---------------------------------------------------------------------------------------
}

//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------

int main(int argc, char **argv)
{
  if (argc == 1 || argc > 2)
  {
    std::cout << usage;
    return 0;
  }

  if (std::string(argv[1]) == "-h")
  {
    std::cout << details;
    return 0;
  }

  std::cout << "Input: " << argv[1] << "\n";

  const auto tokens = ceez::to_tokens(argv[1]);
  assert(tokens.size() == 3);
  auto str1 = tokens[0];
  auto str2 = tokens[1];
  auto str3 = tokens[2];
  std::pair<bool, ceez::mpuint> num1_res, num2_res;

  if (ceez::is_neg(str1))
    num1_res = ceez::string_to_inv_mpuint(str1);
  else
    num1_res = ceez::string_to_mpuint(str1);
  if (!num1_res.first)
  {
    std::cout << "[e] Cannot parse the input number: " << str1 << "\n";
    std::cout << usage;
    std::exit(-3);
  }
  auto num1 = num1_res.second;

  if (ceez::is_neg(str3))
    num2_res = ceez::string_to_inv_mpuint(str3);
  else
    num2_res = ceez::string_to_mpuint(str3);
  if (!num2_res.first)
  {
    std::cout << "[e] Cannot parse the input number: " << str3 << "\n";
    std::cout << usage;
    std::exit(-3);
  }
  auto num2 = num2_res.second;

  auto op_res = ceez::string_to_operation(str2);
  if (!op_res.first)
  {
    std::cout << "[e] Cannot parse the operation: " << str2 << "\n";
    std::cout << usage;
    std::exit(-3);
  }
  auto op = op_res.second;

  // if (argc == 3) // Check if tests are requested
  // {
  //   if (std::string(argv[2]) == "-t") // run tests if asked for
  //   {
  //     ceez::run_tests(num1, num2);
  //     //ceez::run_random_tests();
  //     return 0;
  //   }
  // }

  ceez::mpuint res;
  if (ceez::is_operator_arith(op))
  {
    res = ceez::do_arith(num1, op, num2);
  }
  if (ceez::is_operator_logical(op))
  {
    res = ceez::do_logical(num1, op, num2);
  }
  if (ceez::is_operator_boolean(op))
  {
    if (num2 > 8)
    {
      std::cout << "[e] Max variables allowed is 8. Exiting!\n";
      std::exit(-3);
    }
    auto var = ceez::to_var(num2);

    const ceez::mpuint one = 1;
    if ((one << (var + 1)) >= num1)
    {
      std::cout << "[e] Variable " << num2 << " does not exist in the given truth table. Exiting!\n";
      std::exit(-3);
    }
    res = ceez::do_boolean(num1, op, var);
  }

  std::cout << "Result Dec = " << std::dec << res << "\n";
  std::cout << "       Hex = " << std::hex << res << std::dec << "\n";
  auto bin_str = ceez::mpuint_to_binary_string(res, 512);
  std::cout << "       Bin = " << bin_str
            << "   (" << bin_str.size() << " bits unsigned)"
            << "\n";

  return 0;
}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
