Boolt: Boolean tool for unsigned aribtrary precision operations
===============================================================

> ### Author: Arun <arun@uni-bremen.de>

Why Boolt?
----------
I did not find any useful tool that can convert and do Boolean computations on arbitrarily
precision numbers. For e.g., ` 0xFFF00FFF00FFF00FFFF ^ !0xAAAABBBCCCDDDEEEFFFF `
(first number is 76 bits and second is 80 bits with bitwise xor and complement).
Further boolt can compute cofactors, Boolean difference and other special Boolean 
operations on truth tables.

> For aribtrary precision math functions such as *sqrt()* use **bc** or similar software!

> Max bitsize is limited to 1024. Not *really* arbitrary precision.

> All operations are unsigned


How to build and run?
---------------------

### Dependencies:

* BOOST Libraries ( tried only with version 1.61 )
* C++11 compiler  ( Makeflow uses GCC, and targets a Linux system )
* Gnu Make  ( tried only in 4.1 )

### Build steps:
* make
* sudo make install

### Usage:
* ./boolt

### Supported operations:
* Maths: Multiply ` a * b `, Add ` a + b `, Subtract ` a - b `
* * Subtraction is always absolute value. i.e. `( |a - b| )`
* * Division operation is not even attempted. We are not into floats at all.
* Boolean: And ` a & b `, Or ` a | b `, Xor ` a | b `, Complement ` ~a `
* Truth Table: Cofactor0 ` f cof0 x `, Cofactor1 ` f cof1 x `, Boolean difference ` f diff x `, Boolean inversion ` f inv x `, 
Universal quantification ` f forall x `, Existential quantification ` f exists x `, 
Shannon decomposition ` f shannon x ` (yields the same function, similar to identity).
* * In the above operations, *f* is the truth-table of the function and *x* is the variable (denoted by position from LSB)

Note on Truth Tables
---------------------
Truth tables are compressed form a function *f*

For e.g.,  the truth table for an AND function  ` f = x0 & x1 ` is given below

| x1 | x0 |f  |   
|----|----|---|
|  0 | 0  | 0 |
|  0 | 1  | 0 |
|  1 | 0  | 0 |
|  1 | 1  | 1 |

#### i.e., **f = 1000 = 8** 

Note that the truth table is read from the last row (similar to a MSB) to the first row (similar to LSB)


Similarly the truth tables for cy, s1 and s0 bits (carry and sum) of a 2-bit half adder ` cy:s1:s0 = x1:x0 + y1:y0 ` is

x1 | x0 | y1 | y0 | cy | s1 | s0 
---------|----------|---------|---------|---------|---------|---------
 0 | 0 | 0 | 0 | 0 | 0 | 0  
 0 | 0 | 0 | 1 | 0 | 0 | 1
 0 | 0 | 1 | 0 | 0 | 1 | 0 
 0 | 0 | 1 | 1 | 0 | 1 | 1 
 0 | 1 | 0 | 0 | 0 | 0 | 1 
 0 | 1 | 0 | 1 | 0 | 1 | 0 
 0 | 1 | 1 | 0 | 0 | 1 | 1 
 0 | 1 | 1 | 1 | 1 | 0 | 0 
 1 | 0 | 0 | 0 | 0 | 1 | 0 
 1 | 0 | 0 | 1 | 0 | 1 | 1 
 1 | 0 | 1 | 0 | 1 | 0 | 0 
 1 | 0 | 1 | 1 | 1 | 0 | 1 
 1 | 1 | 0 | 0 | 0 | 1 | 1 
 1 | 1 | 0 | 1 | 1 | 0 | 0 
 1 | 1 | 1 | 0 | 1 | 0 | 1 
 1 | 1 | 1 | 1 | 1 | 1 | 0 

#### which means **s0 = 0101101001011010 = 0x5A5A** and **s1 = 1001001101101100 = 0x936C** and **cy = 1110110010000000 = 0xEC80**

Again note that the truth table is interpreted from the last row to the first row.

#### Truth Tables of common Boolean functions 
* 2 input AND  :  0x8
* 2 input NAND :  0x7
* 2 input OR   :  0xE
* 2 input NOR  :  0x1
* 2 input XOR  :  0x6
* 2 input XNOR :  0x9
* 3 input AND  :  0x80
* 3 input NAND :  0x7F
* 3 input OR   :  0xFE
* 3 input NOR  :  0x01
* 3 input XOR  :  0x96
* 3 input XNOR :  0x69
* 2 input MUX  :  0xE2


#### Variables: The variables are denoted by their position in the truth table, 0 to MSB. 
#### i.e., in the half adder example, y0 is 0,  y1 is 1, x0 is 2, x1 is 3

Special Boolean function operators (TODO)
----------------------------------

> ### An overview is given in doc/notes.pdf

#### Cofactors (TODO)
* Cofactor is the Boolean function obtained when a given variable is forced to take the value 0 (cof0) or value 1 (cof1)
* In the half adder example, cofactor1 of *s0* wrt. *x0* is given to Boolt as **0x5a5a cof1 2**. The solution is **0x5555**

#### Boolean difference (TODO)
* Boolean difference = cof0 ^ cof1
* Boolean difference is very important in testability.

#### Boolean inversion (TODO)
* Boolean inversion = x.cof0 ^ ~x.cof1

#### Shannon (TODO)
* Shannon decomposition = ~x.cof0 ^ x.cof1 
* This is similar to identity, should yield the same function. Given here for completeness.

#### Existential quantification (TODO)
* Existential quantification = cof0 | cof1

#### Universal quantification (TODO)
* Universal quantification = cof0 & cof1
