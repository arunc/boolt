// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
//
// @author : Arun <arun@uni-bremen.de>
// @file   : boolean.hpp
// @brief  : cofator functions
//------------------------------------------------------------------------------

#pragma once
#ifndef BOOLEAN_HPP
#define BOOLEAN_HPP

#include <string>
#include <iostream>
#include <locale>
#include <algorithm>
#include <utility>
#include <vector>

#include <boost/algorithm/string.hpp>

#include "numbers.hpp"

namespace ceez
{
mpuint cofactor0(const mpuint &num1, unsigned var);
mpuint cofactor1(const mpuint &num1, unsigned var);
mpuint difference(const mpuint &num1, unsigned var);
mpuint quantify_forall(const mpuint &num1, unsigned var);
mpuint quantify_exists(const mpuint &num1, unsigned var);
mpuint inversion(const mpuint &num1, unsigned var);
mpuint shannon(const mpuint &num1, unsigned var);
void test_boolean(const mpuint &num1, unsigned var);

inline bool is_operator_boolean(const Op &op)
{
  return (op == Op::COF0) || (op == Op::COF1) || (op == Op::DIFF) ||
         (op == Op::FORALL) || (op == Op::EXISTS) ||
         (op == Op::SHANNON) || (op == Op::INV);
}

// clang-format off
inline mpuint do_boolean(const mpuint &num1, const Op &op, const unsigned var)
{
  mpuint res;
  switch (op)
  {
  case Op::COF0:    res = cofactor0(num1, var);        break;
  case Op::COF1:    res = cofactor1(num1, var);        break;
  case Op::DIFF:    res = difference(num1, var);       break;
  case Op::FORALL:  res = quantify_forall(num1, var);  break;
  case Op::EXISTS:  res = quantify_exists(num1, var);  break;
  case Op::SHANNON: res = shannon(num1, var);          break;
  case Op::INV:     res = inversion(num1, var);        break;
  default:
    assert(false && "Unknown Boolean operation in do_boolean");
  };
  return res;
}
// clang-format on
//---------------------------------------------------------------------------------------
} // namespace ceez

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
