// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
//
// @author : Arun <arun@uni-bremen.de>
// @file   : utils.cpp
// @brief  : common utilities
//------------------------------------------------------------------------------

#include <ctype.h>
#include <cassert>
#include <boost/multiprecision/cpp_int.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/dynamic_bitset.hpp>
#include <boost/algorithm/string/case_conv.hpp>
#include <cstdlib>
#include <random>

#include "utils.hpp"
#include "boolean.hpp"

namespace ceez
{

//---------------------------------------------------------------------------------------
// callees responsibility to check if the input string is valid or not.
// for binary, decimal and hexadecimal.
mpuint string_to_mpuint(const std::string &mystr, const int konst)
{
  auto str = boost::to_lower_copy(mystr, std::locale());
  mpuint num = 0;
  for (char &i : str)
  {
    if (konst == 2 && i == 'b')
      continue;
    switch (i)
    {
    case 'x':
      continue;
    case 'a':
      num = num * konst + 10;
      break;
    case 'b':
      num = num * konst + 11;
      break;
    case 'c':
      num = num * konst + 12;
      break;
    case 'd':
      num = num * konst + 13;
      break;
    case 'e':
      num = num * konst + 14;
      break;
    case 'f':
      num = num * konst + 15;
      break;
    default:
      num = num * konst + (int)(i - '0');
    }
  }
  return num;
}

//---------------------------------------------------------------------------------------
std::string mpuint_to_binary_string(const mpuint &number, const int limit)
{
  if (0 == number)
    return "0";
  std::string numstr;
  int cnt = 0;
  auto num = number;
  while (num > 0)
  {
    auto bit = ((num >> 1) << 1) ^ num;
    if (bit == 0) // note: bit is mpuint, cant convert to string
    {
      numstr.append("0");
    }
    else
    {
      numstr.append({"1"});
    }
    num = num >> 1;
    if (++cnt >= limit) // enough digits.
    {
      std::cout << "[w] Truncating the number of binary digits printed to first "
                << limit << "\n";
      break;
    }
  }
  return std::string(numstr.rbegin(), numstr.rend());
}

// Negation is the bit by bit inversion upto the MSB bit which is 1.
// Leading zeros if any in the input is always ignored. (should not be there)
// 1010 = 0101
// 001010 = 0101 (Note: the result is NOT 110101)
// 0 = 1 (this is an exception)
mpuint negate(const mpuint &number)
{
  auto numstr = mpuint_to_binary_string(number, (sizeof(number) * 8) - 1);
  for (auto i = 0u; i < numstr.size(); i++)
  {
    numstr[i] = (numstr[i] == '0') ? '1' : '0';
  }

  return string_to_mpuint(numstr, 2);
}

unsigned negate(const unsigned var)
{
  return to_var(negate(to_mpuint(var)));
}
//---------------------------------------------------------------------------------------
void print_truthtable(int num_rows)
{
  for (auto i = 0; i < num_rows; i++)
  {
    ceez::mpuint num = i;
    auto res = ceez::mpuint_to_binary_string(num, 512);
    std::cout << boost::format("%10s \n") % res;
  }
}

//---------------------------------------------------------------------------------------
// clang-format off
unsigned num_vars(const mpuint &num)
{
  if (num <= 1)    return 1;
  if (num <= 8)    return 2;
  if (num <= 16)   return 3;
  if (num <= 32)   return 4;
  if (num <= 64)   return 5;
  if (num <= 128)  return 6;
  if (num <= 256)  return 7;
  if (num <= 512)  return 8;
  assert (false && "Cannot deal with this much large truth table.");
  return 0;
}
// clang-format on
unsigned mylog2(const mpuint &num)
{
  auto l = 0;
  auto nn = num;
  while (nn >>= 1)
  {
    ++l;
  }
  return l;
}

void run_tests(const mpuint &num1, const mpuint &num2)
{
  auto max_var1 = num_vars(num1);
  auto max_var2 = num_vars(num2);
  std::random_device rd;  // obtain a random number from hardware
  std::mt19937 eng(rd()); // seed the generator
  std::uniform_int_distribution<> distr1(0, max_var1), distr2(0, max_var2);
  auto var1 = distr1(eng);
  auto var2 = distr2(eng);
  std::cout << "num1 = " << num1 << "  var1 = " << var1 << "  max_var1 = " << max_var1 << "\n";
  std::cout << "num2 = " << num2 << "  var2 = " << var2 << "  max_var2 = " << max_var2 << " \n";
  test_boolean(num1, var1);
  test_boolean(num2, var2);
}

void run_random_tests()
{
  std::random_device rd;  // obtain a random number from hardware
  std::mt19937 eng(rd()); // seed the generator
  std::uniform_int_distribution<> num_distr(0, 256), var_distr(0, 7);
  auto num1 = num_distr(eng);
  auto var1 = var_distr(eng);
  auto num2 = num_distr(eng);
  auto var2 = var_distr(eng);
  auto num3 = num_distr(eng);
  auto var3 = var_distr(eng);
  auto num4 = num_distr(eng);
  auto var4 = var_distr(eng);

  test_boolean(num1, var1);
  test_boolean(num2, var2);
  test_boolean(num3, var3);
  test_boolean(num4, var4);
}
//---------------------------------------------------------------------------------------
}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
