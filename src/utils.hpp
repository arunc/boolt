// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
//
// @author : Arun <arun@uni-bremen.de>
// @file   : utils.hpp
// @brief  : common utilities
//------------------------------------------------------------------------------

#pragma once
#ifndef UTILS_HPP
#define UTILS_HPP

#include <string>
#include <iostream>
#include <locale>
#include <algorithm>
#include <utility>
#include <vector>

#include <boost/algorithm/string.hpp>

#include "numbers.hpp"

namespace ceez
{

mpuint string_to_mpuint(const std::string &mystr, const int konst);
mpuint string_to_inv_mpuint(const std::string &mystr);
std::string mpuint_to_binary_string(const mpuint &number, const int limit);
std::pair<bool, mpuint> string_to_mpuint(const std::string &mystr);
mpuint negate(const mpuint &num);
unsigned negate(const unsigned var);

void print_truthtable(int num_rows);
void run_tests(const mpuint &num1, const mpuint &num2);
void run_random_tests();

//---------------------------------------------------------------------------------------
inline bool is_binary(const std::string &str)
{
  if (str[0] != '0')
    return false;
  if (str[1] != 'b' && str[1] != 'B')
    return false;
  for (const char &i : str)
  {
    if (i != 'b' && i != '0' && i != '1' && i != 'B')
      return false;
  }
  return true;
}

inline bool is_decimal(const std::string &str)
{
  for (const char &i : str)
  {
    if (!std::isdigit(i))
      return false;
  }
  return true;
}

inline bool is_hexadecimal(const std::string &mystr)
{
  if (mystr[0] != '0')
    return false;
  if (mystr[1] != 'x' && mystr[1] != 'X')
    return false;
  auto str = mystr.substr(2);
  for (const char &i : str)
  {
    if (!std::isxdigit(i))
      return false;
  }
  return true;
}

//---------------------------------------------------------------------------------------
// bool returned is true only if the conversion is succeeded. i.e., input string valid.
inline std::pair<bool, mpuint> string_to_mpuint(const std::string &str)
{
  if (is_binary(str))
  {
    return {true, string_to_mpuint(str, 2)};
  }
  if (is_hexadecimal(str))
  {
    return {true, string_to_mpuint(str, 16)};
  }
  if (is_decimal(str))
  {
    return {true, string_to_mpuint(str, 10)};
  }
  return {false, 0};
}

//---------------------------------------------------------------------------------------
inline std::pair<bool, mpuint> string_to_inv_mpuint(std::string &mystr)
{
  assert(is_neg(mystr));
  boost::erase_all(mystr, "-");
  boost::erase_all(mystr, "~");
  auto num_res = string_to_mpuint(mystr);
  return {num_res.first, negate(num_res.second)};
}

//---------------------------------------------------------------------------------------
inline std::vector<std::string> to_tokens(char *argv)
{
  auto str = std::string(argv);
  std::vector<std::string> tok_ens, tokens;
  boost::split(tok_ens, str, boost::is_any_of(" "));
  for (auto &str : tok_ens) // prune empty strings
  {
    if (str != "")
      tokens.emplace_back(std::move(str));
  }
  return tokens;
}

//---------------------------------------------------------------------------------------

} // namespace ceez

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
