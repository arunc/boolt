// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
//
// @author : Arun <arun@uni-bremen.de>
// @file   : cofactor.cpp
// @brief  : cofactor functions
//------------------------------------------------------------------------------

#include <ctype.h>
#include <cassert>
#include <boost/multiprecision/cpp_int.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/dynamic_bitset.hpp>
#include <boost/algorithm/string/case_conv.hpp>
#include <cstdlib>

#include "boolean.hpp"
#include "logical.hpp"
#include "utils.hpp"

namespace ceez
{

mpuint cofactor0(const mpuint &num1, unsigned var)
{
  assert(var <= cof0.size() && "#vars exceeded limit");
  auto num2 = string_to_mpuint(cof0[var].second, 16);
  auto tmp = num1 & num2;
  auto res = tmp | (tmp << cof0[var].first);
  return res;
}

mpuint cofactor1(const mpuint &num1, unsigned var)
{
  assert(var <= cof1.size() && "#vars exceeded limit");
  auto num2 = string_to_mpuint(cof1[var].second, 16);
  auto tmp = num1 & num2;
  auto res = tmp | (tmp >> cof1[var].first);
  return res;
}

mpuint difference(const mpuint &num1, unsigned var)
{
  auto cf0 = cofactor0(num1, var);
  auto cf1 = cofactor1(num1, var);
  auto res = cf0 ^ cf1;
  return res;
}

mpuint quantify_forall(const mpuint &num1, unsigned var)
{
  auto cf0 = cofactor0(num1, var);
  auto cf1 = cofactor1(num1, var);
  auto res = cf0 & cf1;
  return res;
}

mpuint quantify_exists(const mpuint &num1, unsigned var)
{
  auto cf0 = cofactor0(num1, var);
  auto cf1 = cofactor1(num1, var);
  auto res = cf0 | cf1;
  return res;
}

mpuint shannon(const mpuint &num1, unsigned var)
{
  auto cf0 = cofactor0(num1, var);
  auto cf1 = cofactor1(num1, var);
  mpuint v0 = string_to_mpuint(cof0[var].second, 16);
  mpuint v1 = string_to_mpuint(cof1[var].second, 16);
  auto res = (v0 & cf0) | (v1 & cf1);
  return res;
}

mpuint inversion(const mpuint &num1, unsigned var)
{
  auto cf0 = cofactor0(num1, var);
  auto cf1 = cofactor1(num1, var);
  mpuint v0 = string_to_mpuint(cof0[var].second, 16);
  mpuint v1 = string_to_mpuint(cof1[var].second, 16);
  auto res = (v1 & cf0) | (v0 & cf1);
  return res;
}

// TODO: put more tests here.
// Learn and get boost tests here.
void test_boolean(const mpuint &num1, unsigned var)
{
  auto cf0 = cofactor0(num1, var);
  auto cf1 = cofactor1(num1, var);
  //auto dff = difference(num1, var);
  auto shan = shannon(num1, var);
  auto inv = inversion(num1, var);
  //auto qbf_exists = quantify_exists(num1, var);
  //auto qbf_forall = quantify_forall(num1, var);

  std::cout << "## num = " << num1 << "  var = " << var << "\n";
  // test1 : cofactor of a complement = complement of the cofactor
  // Note: cannot pass information to more than one level.
  // For e.g., if cofactor0 is 0, then its complement is taken as 1, where as 
  // depending on the situation it might be F, FF etc.
  //--not unique-- auto test1_rhs0 = negate(cf0);
  //--not unique-- auto test1_lhs0 = cofactor0(negate(num1), var);
  //--not unique-- auto test1_rhs1 = negate(cf1);
  //--not unique-- auto test1_lhs1 = cofactor1(negate(num1), var);

  // test2 : cofactor of an operation = operation of the cofactors
  // TODO

  // test3 : shannon is identity

  // test4 : inversion of inversion is identity.
  auto test4 = inversion(inv, var);

  // Check the status
  //---not unique-- std::string status = (test1_lhs0 == test1_rhs0) ? "PASS" : "FAIL";
  //---not unique-- std::cout << "test1 : " << test1_lhs0 << " = " << test1_rhs0 << " : " << status << "\n";
  //---not unique-- status = (test1_lhs1 == test1_rhs1) ? "PASS" : "FAIL";
  //---not unique-- std::cout << "test1 : " << test1_lhs1 << " = " << test1_rhs1 << " : " << status << "\n";
  status = (shan == num1) ? "PASS" : "FAIL";
  std::cout << "test3 : " << shan << " = " << num1 << " : " << status << "\n";
  status = (test4 == num1) ? "PASS" : "FAIL";
  std::cout << "test4 : " << test4 << " = " << num1 << " : " << status << "\n";
}

//---------------------------------------------------------------------------------------
}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
