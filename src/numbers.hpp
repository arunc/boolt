// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
//
// @author : Arun <arun@uni-bremen.de>
// @file   : numbers.hpp
// @brief  : basic number types and operators
//------------------------------------------------------------------------------

#pragma once
#ifndef NUMBERS_HPP
#define NUMBERS_HPP

#include <utility>
#include <algorithm>
#include <boost/multiprecision/cpp_int.hpp>
#include <boost/multiprecision/cpp_dec_float.hpp>

namespace ceez
{
using mpuint = boost::multiprecision::uint1024_t;
using mpfloat = boost::multiprecision::cpp_dec_float_100;

// clang-format off
inline unsigned to_var(const mpuint &num)
{
  assert(num < 9 && "Max number of variables is 8");
  if (num == 0) return 0u;
  if (num == 1) return 1u;
  if (num == 2) return 2u;
  if (num == 3) return 3u;
  if (num == 4) return 4u;
  if (num == 5) return 5u;
  if (num == 6) return 6u;
  if (num == 7) return 7u;
  if (num == 8) return 8u;
  return 0;
}

inline mpuint to_mpuint(const unsigned var)
{
  assert(var < 9 && "Max number of variables is 8");
  mpuint num = var;
  return num;
}

// Notes: "f shannon var = f", this is the identity/test operator.
enum class Op
{
  ADD,     // addition
  SUB,     // subtraction, but always absolute value, |a-b|
  MULT,    // multiplication
  DIV,     // division, not supported, for the sake of completeness
  AND,     // Boolean AND                     (&)
  OR,      // Boolean OR                      (|)
  XOR,     // Boolean XOR                     (^)
  NEG,     // Boolean NEGATION                (!)
  COF0,    // Boolean Cofactor0               (f cof0 var)
  COF1,    // Boolean Cofactor1               (f cof1 var)
  DIFF,    // Boolean Difference              (f diff var)
  FORALL,  // Boolean Universal Quantifier    (f forall var)
  EXISTS,  // Boolean Existential Quantifier  (f exists var)
  SHANNON, // Shannon expansion               (f shannon var)
  INV      // Boolean Inversion               (f inv  var)
};

// clang-format off
// enum to string
inline std::string to_string(const Op &op)
{
  if (op == Op::ADD)       return "ADD";
  if (op == Op::SUB)       return "SUB";
  if (op == Op::MULT)      return "MULT";
  if (op == Op::DIV)       return "DIV";
  if (op == Op::AND)       return "AND";
  if (op == Op::OR)        return "OR";
  if (op == Op::XOR)       return "XOR";
  if (op == Op::NEG)       return "NEG";
  if (op == Op::COF0)      return "COF0";
  if (op == Op::COF1)      return "COF1";
  if (op == Op::DIFF)      return "DIFF";
  if (op == Op::FORALL)    return "FORALL";
  if (op == Op::EXISTS)    return "EXISTS";
  if (op == Op::SHANNON)   return "SHANNON";
  if (op == Op::INV)       return "INV";
  return "UNKNOWN";
}

//---------------------------------------------------------------------------------------
// string to enum and predicates
inline bool is_divide(const std::string &str)
{
  if (str[0] == '/' && str.size() == 1)   assert(false && "Division not supported");
  return false;
}
inline bool is_neg(const std::string &str)
{
  return (str[0] == '~' || str[0] == '-') && str.size() > 1;
}

inline bool is_add(const std::string &str)      { return str[0] == '+' && str.size() == 1; }
inline bool is_subtract(const std::string &str) { return str[0] == '-' && str.size() == 1; }
inline bool is_multiply(const std::string &str) { return str[0] == '*' && str.size() == 1; }
inline bool is_and(const std::string &str)      { return str[0] == '&' && str.size() == 1; }
inline bool is_or(const std::string &str)       { return str[0] == '|' && str.size() == 1; }
inline bool is_xor(const std::string &str)      { return str[0] == '^' && str.size() == 1; }
inline bool is_cof0(const std::string &str)     { return str == "cof0"; }
inline bool is_cof1(const std::string &str)     { return str == "cof1"; }
inline bool is_diff(const std::string &str)     { return str == "diff"; }
inline bool is_forall(const std::string &str)   { return str == "forall"; }
inline bool is_exists(const std::string &str)   { return str == "exists"; }
inline bool is_shannon(const std::string &str)  { return str == "shannon"; }
inline bool is_inv(const std::string &str)      { return str == "inv"; }

//---------------------------------------------------------------------------------------
// bool returned is true only if the conversion is succeeded. i.e., input string valid.
inline std::pair<bool, Op> string_to_operation(const std::string &str)
{
  if (is_add(str))         return {true, Op::ADD};
  if (is_subtract(str))    return {true, Op::SUB};
  if (is_multiply(str))    return {true, Op::MULT};
  if (is_divide(str))      return {true, Op::DIV};
  if (is_and(str))         return {true, Op::AND};
  if (is_or(str))          return {true, Op::OR};
  if (is_xor(str))         return {true, Op::XOR};
  if (is_diff(str))        return {true, Op::DIFF};
  if (is_cof0(str))        return {true, Op::COF0};
  if (is_cof1(str))        return {true, Op::COF1};
  if (is_forall(str))      return {true, Op::FORALL};
  if (is_exists(str))      return {true, Op::EXISTS};
  if (is_shannon(str))     return {true, Op::SHANNON};
  if (is_inv(str))         return {true, Op::INV};

  return {false, Op::ADD}; // Op::ADD is a dummy placeholder
}
// clang-format on

//------------------------------------------------------------------------------
// Cofactor manipulations
// Temporary variables
static const std::string _cof1_0 = "A";
static const std::string _cof1_1 = "C";
static const std::string _cof1_2 = "F0";
static const std::string _cof1_3 = "FF00";
static const std::string _cof1_4 = "FFFF0000";
static const std::string _cof1_5 = "FFFFFFFF00000000";
static const std::string _cof1_6 = "FFFFFFFFFFFFFFFF0000000000000000";
static const std::string _cof1_7 = "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000000000000000000000000000";
static const std::string _cof1_8 = "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000000000000000000000000000000000000000000000000000000000000";

static const std::string _cof0_0 = "5";
static const std::string _cof0_1 = "3";
static const std::string _cof0_2 = "0F";
static const std::string _cof0_3 = "00FF";
static const std::string _cof0_4 = "0000FFFF";
static const std::string _cof0_5 = "00000000FFFFFFFF";
static const std::string _cof0_6 = "0000000000000000FFFFFFFFFFFFFFFF";
static const std::string _cof0_7 = "00000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF";
static const std::string _cof0_8 = "0000000000000000000000000000000000000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF";

inline std::string operator*(const std::string &str, unsigned n) // std::string("aa") * 10
{
  std::string newstr;
  newstr.reserve(n * str.size());
  for (auto i = 0u; i < n; i++)
  {
    newstr = newstr + str;
  }
  return newstr;
}
inline std::string operator*(std::string &str, unsigned n) // "aa" * 10
{
  std::string newstr;
  newstr.reserve(n * str.size());
  for (auto i = 0u; i < n; i++)
  {
    newstr = newstr + str;
  }
  return newstr;
}

// #bits = sizeof(mpuint) * 8
// #repetitions = #bits / size_of_string

// first is the shift and second is the constant. Support upto 8 variables.
// clang-format off
static const std::pair<unsigned, std::string> cof1_0 = {1,    _cof1_0  * (int) ( (sizeof(mpuint) * 8) / _cof1_0.size()) };
static const std::pair<unsigned, std::string> cof1_1 = {2,    _cof1_1  * (int) ( (sizeof(mpuint) * 8) / _cof1_1.size()) };
static const std::pair<unsigned, std::string> cof1_2 = {4,    _cof1_2  * (int) ( (sizeof(mpuint) * 8) / _cof1_2.size()) };
static const std::pair<unsigned, std::string> cof1_3 = {8,    _cof1_3  * (int) ( (sizeof(mpuint) * 8) / _cof1_3.size()) };
static const std::pair<unsigned, std::string> cof1_4 = {16,   _cof1_4  * (int) ( (sizeof(mpuint) * 8) / _cof1_4.size()) };
static const std::pair<unsigned, std::string> cof1_5 = {32,   _cof1_5  * (int) ( (sizeof(mpuint) * 8) / _cof1_5.size()) };
static const std::pair<unsigned, std::string> cof1_6 = {64,   _cof1_6  * (int) ( (sizeof(mpuint) * 8) / _cof1_6.size()) };
static const std::pair<unsigned, std::string> cof1_7 = {128,  _cof1_7  * (int) ( (sizeof(mpuint) * 8) / _cof1_7.size()) };
static const std::pair<unsigned, std::string> cof1_8 = {256,  _cof1_8  * (int) ( (sizeof(mpuint) * 8) / _cof1_8.size()) };

static const std::pair<unsigned, std::string> cof0_0 = {1,    _cof0_0  * (int) ( (sizeof(mpuint) * 8) / _cof0_0.size()) };
static const std::pair<unsigned, std::string> cof0_1 = {2,    _cof0_1  * (int) ( (sizeof(mpuint) * 8) / _cof0_1.size()) };
static const std::pair<unsigned, std::string> cof0_2 = {4,    _cof0_2  * (int) ( (sizeof(mpuint) * 8) / _cof0_2.size()) };
static const std::pair<unsigned, std::string> cof0_3 = {8,    _cof0_3  * (int) ( (sizeof(mpuint) * 8) / _cof0_3.size()) };
static const std::pair<unsigned, std::string> cof0_4 = {16,   _cof0_4  * (int) ( (sizeof(mpuint) * 8) / _cof0_4.size()) };
static const std::pair<unsigned, std::string> cof0_5 = {32,   _cof0_5  * (int) ( (sizeof(mpuint) * 8) / _cof0_5.size()) };
static const std::pair<unsigned, std::string> cof0_6 = {64,   _cof0_6  * (int) ( (sizeof(mpuint) * 8) / _cof0_6.size()) };
static const std::pair<unsigned, std::string> cof0_7 = {128,  _cof0_7  * (int) ( (sizeof(mpuint) * 8) / _cof0_7.size()) };
static const std::pair<unsigned, std::string> cof0_8 = {256,  _cof0_8  * (int) ( (sizeof(mpuint) * 8) / _cof0_8.size()) };
// clang-format on

// this vector is all we need for further manipulations.
static const std::vector<std::pair<unsigned, std::string>> cof0 = {cof0_0, cof0_1, cof0_2, cof0_3, cof0_4, cof0_5, cof0_6, cof0_7, cof0_8};
static const std::vector<std::pair<unsigned, std::string>> cof1 = {cof1_0, cof1_1, cof1_2, cof1_3, cof1_4, cof1_5, cof1_6, cof1_7, cof1_8};

//------------------------------------------------------------------------------
} // namespace ceez

#endif
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
