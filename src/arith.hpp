// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
//
// @author : Arun <arun@uni-bremen.de>
// @file   : arith.hpp
// @brief  : Arithmetic functions on two mpuint/mpfloat numbers.
//------------------------------------------------------------------------------

#pragma once
#ifndef ARITH_HPP
#define ARITH_HPP

#include <cassert>

#include "numbers.hpp"

namespace ceez
{
inline bool is_operator_arith(const Op &op)
{
  return (op == Op::ADD) || (op == Op::SUB) || (op == Op::MULT) || (op == Op::DIV);
}

inline mpuint do_arith(const mpuint &num1, const Op &op, const mpuint &num2)
{
  mpuint res;
  switch (op)
  {
  case Op::ADD:
    res = num1 + num2;
    break;
  case Op::SUB:
    res = (num1 > num2) ? (num1 - num2) : (num2 - num1);
    break;
  case Op::MULT:
    res = num1 * num2;
    break;
  case Op::DIV:
    assert(false && "Division not supported");
    break;
  default:
    assert(false && "Unknown arithmetic operation in do_arith");
  };
  return res;
}

} // namespace ceez

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
