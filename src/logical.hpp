// Copyright (C) AGRA - University of Bremen
//
// LICENSE : Eclipse Public License (EPL 1.0)
//
// @author : Arun <arun@uni-bremen.de>
// @file   : logical.hpp
// @brief  : logical functions on two mpuint/mpfloat numbers.
//------------------------------------------------------------------------------

#pragma once
#ifndef LOGICAL_HPP
#define LOGICAL_HPP

#include "numbers.hpp"

namespace ceez
{

inline bool is_operator_logical(const Op &op)
{
  return (op == Op::AND) || (op == Op::OR) || (op == Op::XOR);
}
inline bool is_operator_negate(const Op &op)
{
  return (op == Op::NEG);
}

inline mpuint do_logical(const mpuint &num1, const Op &op, const mpuint &num2)
{
  mpuint res;
  switch (op)
  {
  case Op::OR:
    res = num1 | num2;
    break;
  case Op::AND:
    res = num1 & num2;
    break;
  case Op::XOR:
    res = num1 ^ num2;
    break;
  default:
    assert(false && "Unknown logical operation in do_logical");
  };
  return res;
}

} // namespace ceez

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
