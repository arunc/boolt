

SHELL=/bin/bash
base: main.cpp
	g++ -Wall -std=c++11 -Isrc src/utils.cpp src/boolean.cpp main.cpp -o boolt

install: boolt
	cp -f boolt /usr/bin/

clean:
	rm -f boolt

